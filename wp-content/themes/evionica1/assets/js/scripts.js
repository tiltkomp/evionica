(function ($) {

    var existCondition = setInterval(function () {
        if ($('#nv-primary-navigation').length) {
            clearInterval(existCondition);
            $('.sub-menu').addClass('hide');


            $("#menu-item-647 > a").click(function (e) {
                e.preventDefault();
            });
            $("#menu-item-699 > a").click(function (e) {
                e.preventDefault();
            });

            $('#nv-primary-navigation > li:nth-child(5)').addClass('phone');

            $("#nv-primary-navigation > li").on('click', function () {
                if($('.sub-menu', this).hasClass('hide')) {
                    $('.sub-menu').removeClass('show').addClass('hide');

                    $('.sub-menu', this).removeClass('hide').addClass('show');

                    $("#nv-primary-navigation > li").removeClass('active');
                    $(this).addClass('active');
                } else{

                    $('.sub-menu', this).addClass('hide').removeClass('show');
                    $(this).removeClass('active');
                }
            });
            $('.neve-main').on('click', function () {

                $('.sub-menu').each(function () {
                    $(this).removeClass('show').addClass('hide');
                });
                $('.primary-menu-ul > li').each(function () {
                    $(this).removeClass('active');
                });

            });

            $('#nv-primary-navigation').unbind('mouseenter mouseleave');

            $(window).scroll(function () {
                if ($(this).scrollTop() > 400) {
                    $("#header-grid").addClass('affix');
                } else {
                    $("#header-grid").removeClass('affix');
                }
            });

        }
    }, 100);


})(jQuery);