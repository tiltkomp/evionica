<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'evionica' );

/** MySQL database username */
define( 'DB_USER', 'elmo' );

/** MySQL database password */
define( 'DB_PASSWORD', 'test' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sw@>7tWbp;J9;=Cg5nn3Lgi1#@?JT=.g^ksl*%SUQj~B/iC f2-%O_;^ium(SyS0' );
define( 'SECURE_AUTH_KEY',  'f]nPMCdmcaOG/!Q3j%Sgb<i-n|A]@>7ZyV6{u~[DU;#J!PSFcU(.(dG^3^;PTIaz' );
define( 'LOGGED_IN_KEY',    'WR!G 3pLHr^qpVJ1hwUfUh.W;6:xeC+Y?U2d>:8/O%.%FY|e630xZnU*eP$*s>z9' );
define( 'NONCE_KEY',        '>Cn-]{@#tG`J{WnJdm%6 ij9lgPQgA.x@2XN6XC|#lK/X9*#6o!?D^~BknkA8}^e' );
define( 'AUTH_SALT',        '4f7Dgf~nvpX]X:6=5pr4Y?O^lZ8VjH}#C)~0Oxr*/O5zXkq~Bn@JQ9%TXJ?QKjwP' );
define( 'SECURE_AUTH_SALT', 'b92Eto%&CG#{$`Z?pH51|EM!~q#X9M]~W9w]G*]N)34y:E}UK( gpXeZW6IP+P{k' );
define( 'LOGGED_IN_SALT',   '|GEF*je2Z%bt?v}>F.gGRm>9{XWT9oXu.YG)MS;ZMJ7o8J+#9!.3tZW^Uo!!KjJ4' );
define( 'NONCE_SALT',       'dZ%.l+6I,BL=4w9DJS|?-zht,]2a#sQ;G4liYP(^Pnu#UF>LJi(j%]<D0;/X8n8`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );


define('ALLOW_UNFILTERED_UPLOADS', true);